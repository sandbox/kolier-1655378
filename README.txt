7.x-1.x
  Try to attach a field to the entity edit form to detect the trigger status.
7.x-2.x
  Try to use input filter to detect input format that need to load MathJax.